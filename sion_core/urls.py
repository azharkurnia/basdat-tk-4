from django.conf.urls import url
from .views import *


urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^register/', register, name='register'),
    url(r'^login/', login, name='login'),
    url(r'^logout/', logout, name='logout'),
    url(r'^register-sponsor/', register_sponsor, name='register_sponsor'),
    url(r'^register-volunteer/', register_volunteer, name='register_volunteer'),
    url(r'^register-donator/', register_donator, name='register_donator'),
    url(r'^register-organisasi/', register_organisasi, name='register-organisasi'),
    url(r'^profil-user/', profile_user, name='profil-user'),
    url(r'^profil-organisasi/', profile_org, name='profil-organisasi'),
    url(r'^profile-search/', profile_search, name='profil-org-search'),
    url(r'^donasi-organisasi/', donasi_org, name='donasi-organisasi'),
    url(r'^profil-donator-sponsor/', profile_user, name='profil-donator-sponsor'),


]
