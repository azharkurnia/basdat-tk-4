from django.apps import AppConfig


class SionCoreConfig(AppConfig):
    name = 'sion_core'
