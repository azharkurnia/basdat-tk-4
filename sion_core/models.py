from django.db import models, connections   

# # Create your models here.
# class Pengguna(models.Model):
#     email = models.EmailField(unique=True, primary_key=True)
#     password = models.CharField(max_length=50)
#     nama = models.CharField(max_length=100)
#     alamat_lengkap = models.TextField()

#     class Meta:
#      db_table = 'pengguna'

# class Donatur(models.Model):
#     email = models.OneToOneField(Pengguna, primary_key=True, on_delete=models.CASCADE, db_column='email')
#     saldo = models.IntegerField(default=0)

#     class Meta:
#         db_table = 'donatur'


# class Sponsor(models.Model):
#     email = models.OneToOneField(Pengguna,  primary_key=True, on_delete=models.CASCADE, db_column='email')
#     logo_sponsor = models.CharField(max_length=100)

#     class Meta:
#         db_table = 'sponsor'


# class Relawan(models.Model):
#     email = models.OneToOneField(Pengguna, primary_key=True, on_delete=models.CASCADE, db_column='email')
#     no_hp = models.CharField(max_length=20)
#     tanggal_lahir = models.DateField()

#     class Meta:
#         db_table = 'relawan'


# class KeahlianRelawan(models.Model):
#     email = models.ForeignKey(Relawan, on_delete=models.CASCADE, db_column='email')
#     keahlian = models.CharField(max_length=50)

#     class Meta:
#         db_table = 'keahlian_relawan'
#         unique_together = (('email', 'keahlian'),)


# class Organisasi(models.Model):
#     email_organisasi = models.CharField(primary_key=True, max_length=50)
#     website = models.CharField(max_length=50)
#     nama = models.CharField(max_length=50)
#     provinsi = models.CharField(max_length=50)
#     kabupaten_kota = models.CharField(max_length=50)
#     kecamatan = models.CharField(max_length=50)
#     kelurahan = models.CharField(max_length=50)
#     kode_pos = models.CharField(max_length=50)
#     status_verifikasi = models.CharField(max_length=50)

#     class Meta:
#         db_table = 'organisasi'

# class PengurusOrganisasi(models.Model):
#     email = models.OneToOneField(Pengguna, on_delete=models.CASCADE, db_column='email')
#     organisasi = models.ForeignKey(Organisasi, on_delete=models.CASCADE, db_column='organisasi')

#     class Meta:
#         db_table = 'pengurus_organisasi'