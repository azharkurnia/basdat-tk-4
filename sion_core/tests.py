from django.test import TestCase, Client
from django.urls import resolve
from django.shortcuts import reverse
# Create your tests here.
class TestCore(TestCase):

    def test_core_index_exists(self):
            response = Client().get('/')
            self.assertEqual(response.status_code, 200)