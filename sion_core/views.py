from django.shortcuts import render, redirect, reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.db import connection

# Create your views here.
response = {}

def index(request):
    return render(request, 'core_index.html')

def register(request):
    if 'logged_in' not in request.session or not request.session['logged_in']:
        return render(request, 'core_register.html')
        
    messages.info(request, "Anda sudah terdaftar dalam sistem. Silahkan logout terlebih dahulu!")
    return redirect(reverse('core:index'))
    

@csrf_exempt
def login(request):
    response = {}
    if 'logged_in' not in request.session or not request.session['logged_in']:
        if request.method == 'POST':
            cursor=connection.cursor()
            email = request.POST['email']
            password = request.POST['password']
            cursor.execute("SELECT * from SION.PENGGUNA where EMAIL='"+email+"'")
            select=cursor.fetchone()
            if select:
                cursor.execute("SELECT password from SION.PENGGUNA where EMAIL='"+email+"'")
                select=cursor.fetchone()
                if select[0] == password:
                    request.session['logged_in'] = True
                    request.session.modified = True
                    request.session["email"]=email

                    cursor.execute("SELECT nama from SION.PENGGUNA where EMAIL='"+email+"'")
                    select=cursor.fetchone()
                    request.session["nama"]=select[0]

                    cursor.execute("SELECT * from SION.RELAWAN where EMAIL='"+email+"'")
                    select=cursor.fetchone()
                    if (select):
                        request.session["role"]="relawan"
                        messages.success(request, "Anda telah berhasil login kedalam sistem")
                        return redirect(reverse('core:index'))

                    cursor.execute("SELECT * from SION.DONATUR where EMAIL='"+email+"'")
                    select=cursor.fetchone()
                    if (select):
                        request.session["role"]="donatur"
                        messages.success(request, "Anda telah berhasil login kedalam sistem")
                        return redirect(reverse('core:index'))

                    cursor.execute("SELECT * from SION.SPONSOR where EMAIL='"+email+"'")
                    select=cursor.fetchone()
                    if (select):
                        request.session["role"]="sponsor"
                        messages.success(request, "Anda telah berhasil login kedalam sistem")
                        return redirect(reverse('core:index'))

                    #IMPLEMENT PENGURUS HERE AND ORGANISASI
                    cursor.execute("SELECT * from SION.PENGURUS_ORGANISASI where EMAIL='"+email+"'")
                    select=cursor.fetchone()
                    if (select):
                        request.session["role"]="pengurus"
                        messages.success(request, "Anda telah berhasil login kedalam sistem")
                        return redirect(reverse('core:index'))

                else:
                    messages.error(request, 'Incorrect email or password.')
                    response['email'] = request.POST['email']
                    return render(request, 'core_login.html', response)
            else:
                messages.error(request, 'Incorrect email or password.')
                response['email'] = request.POST['email']
                return render(request, 'core_login.html', response)
        else:
            return render(request, 'core_login.html')

    else:
        messages.info(request,'Anda sudah masuk kedalam sistem.')
        return redirect(reverse('core:index'))
                    
def logout(request):
    messages.success(request, 'Anda berhasil keluar dari sistem.')
    request.session.flush()
    return redirect(reverse('core:index'))

@csrf_exempt
def register_sponsor(request):
    if 'logged_in' not in request.session or not request.session['logged_in']:
        if(request.method == 'POST'):
            name = request.POST["name"]
            email = request.POST["email"]
            pwd = request.POST["password"]
            address = request.POST["alamat"]
            logo = request.POST["pic"]
            kecamatan = request.POST["kecamatan"]
            kabupaten = request.POST["kabupaten"]
            kodepos = request.POST["kodepos"]

            alamat_lengkap = address+', '+ kecamatan +', '+kabupaten+', '+kodepos

            cursor = connection.cursor()
            cursor.execute("SELECT * from SION.PENGGUNA where EMAIL='"+email+"'")
            select = cursor.fetchone()
            if (select):
                messages.error(request, 'Email already registered.')
                return render(request,'register_sponsor.html',response) 
            cursor.execute("INSERT INTO SION.PENGGUNA (email,password,nama,alamat_lengkap) VALUES('"+email+"','"+pwd+"','"+name+"','"+alamat_lengkap+"')")
            cursor.execute("INSERT INTO SION.SPONSOR (email,logo_sponsor) VALUES('"+email+"','"+logo+"')")

            request.session["email"] = email
            request.session["name"] = name
            request.session["role"] = "sponsor"
            request.session["logo"] = logo
            request.session["address"] = alamat_lengkap
            request.session["logged_in"] = True
            request.session.modified = True

            messages.success(request, "Anda berhasil registrasi sebagai sponsor")
            return redirect(reverse('core:index'))
        return render(request, 'register_sponsor.html')
    messages.info(request, "Anda sudah terdaftar dalam sistem. Silahkan logout terlebih dahulu!")
    return redirect(reverse('core:index'))

@csrf_exempt
def register_volunteer(request):
    if 'logged_in' not in request.session or not request.session['logged_in']:
        if(request.method == 'POST'):
            name = request.POST["name"]
            email = request.POST["email"]
            pwd = request.POST["password"]
            address = request.POST["alamat"]
            kecamatan = request.POST["kecamatan"]
            kabupaten = request.POST["kabupaten"]
            kodepos = request.POST["kodepos"]
            ttl = request.POST["ttl"]
            keahlian = request.POST["keahlian"]
            telpon = request.POST["telpon"]

            alamat_lengkap = address+', '+ kecamatan +', '+kabupaten+', '+kodepos


            cursor = connection.cursor()
            cursor.execute("SELECT * from SION.PENGGUNA where EMAIL='"+email+"'")
            select = cursor.fetchone()
            if (select):
                messages.error(request, 'FAILED! Email already registered.')
                return render(request,'register_volunteer.html',response) 
            cursor.execute("INSERT INTO SION.PENGGUNA (email,password,nama,alamat_lengkap) VALUES('"+email+"','"+pwd+"','"+name+"','"+alamat_lengkap+"')")
            cursor.execute("INSERT INTO SION.RELAWAN (email, no_hp, tanggal_lahir) VALUES('"+email+"','"+telpon+"','"+ttl+"')")
            keahlian = keahlian.split(";")
            for i in keahlian:
                cursor.execute("INSERT INTO SION.KEAHLIAN_RELAWAN (email, keahlian)  VALUES('"+email+"','"+i+"')")

            request.session["email"] = email
            request.session["name"] = name
            request.session["role"] = "relawan"
            request.session["address"] = alamat_lengkap
            request.session["logged_in"] = True
            request.session["keahlian"] = keahlian
            request.session["ttl"] = ttl
            request.session["telpon"] = telpon
            request.session.modified = True

            messages.success(request,"Anda berhasil registrasi sebagai relawan")
            return redirect(reverse('core:index'))
        return render(request, 'register_volunteer.html')
    messages.warning(request,"Anda sudah terdaftar dalam sistem. Silahkan logout terlebih dahulu!")
    return redirect(reverse('core:index'))

@csrf_exempt
def register_donator(request):
    if 'logged_in' not in request.session or not request.session['logged_in']:
        if(request.method == 'POST'):
            name = request.POST["name"]
            email = request.POST["email"]
            pwd = request.POST["password"]
            address = request.POST["alamat"]
            kecamatan = request.POST["kecamatan"]
            kabupaten = request.POST["kabupaten"]
            kodepos = request.POST["kodepos"]

            alamat_lengkap = address+', '+ kecamatan +', '+kabupaten+', '+kodepos

            cursor = connection.cursor()
            cursor.execute("SELECT * from SION.PENGGUNA where EMAIL='"+email+"'")
            select = cursor.fetchone()
            if (select):
                messages.error(request, 'Email already registered.')
                return render(request,'register_sponsor.html',response) 
            cursor.execute("INSERT INTO SION.PENGGUNA (email,password,nama,alamat_lengkap) VALUES('"+email+"','"+pwd+"','"+name+"','"+alamat_lengkap+"')")
            cursor.execute("INSERT INTO SION.DONATUR (email,saldo) VALUES('"+email+"','0')")


            request.session["email"] = email
            request.session["name"] = name
            request.session["role"] = "donatur"
            request.session["address"] = alamat_lengkap
            request.session["logged_in"] = True
            request.session.modified = True

            messages.success(request,"Anda berhasil registrasi sebagai donatur")
            return redirect(reverse('core:index'))
        return render(request, 'register_donator.html')
    messages.warning(request,"Anda sudah terdaftar dalam sistem. Silahkan logout terlebih dahulu!")
    return redirect(reverse('core:index'))

@csrf_exempt
def register_organisasi(request):
    if 'logged_in' not in request.session or not request.session['logged_in']:
        if(request.method == 'POST'):
            org_name = request.POST["org_name"]
            website = request.POST["website"]
            org_email = request.POST["org_email"]
            address = request.POST["alamat"]
            kecamatan = request.POST["kecamatan"]
            kabupaten = request.POST["kabupaten"]
            kodepos = request.POST["kodepos"]
            tujuan = request.POST["tujuan"]
            adm_name = request.POST["nama_pengurus"]
            adm_pwd = request.POST["password_pengurus"]
            adm_email = request.POST["email_pengurus"]
            adm_address = request.POST["alamat_pengurus"]
            temp = address.split("/")
            kelurahan = temp[2]
            status_verifikasi = False

            alamat_lengkap = address+', '+ kecamatan +', '+kabupaten+', '+kodepos

            cursor = connection.cursor()
            cursor.execute("SELECT * from SION.ORGANISASI where email_organisasi='"+org_email+"'")
            select = cursor.fetchone()
            if (select):
                messages.error(request, 'Email already registered.')
                return render(request,'register_organisasi.html',response)

            cursor.execute("SELECT * from SION.PENGURUS_ORGANISASI where email='"+adm_email+"'")
            select = cursor.fetchone()
            if (select):
                messages.error(request, 'Email already registered.')
                return render(request,'register_organisasi.html',response)

            cursor.execute("INSERT INTO SION.PENGGUNA (email,password,nama,alamat_lengkap) VALUES('"+adm_email+"','"+adm_pwd+"','"+adm_name+"','"+adm_address+"')")
            cursor.execute("INSERT INTO SION.PENGURUS_ORGANISASI (email,organisasi) VALUES('"+adm_email+"','"+org_email+"')")
            cursor.execute("INSERT INTO SION.ORGANISASI (email_organisasi,website,nama,provinsi,kabupaten_kota,kecamatan,kelurahan,kode_pos,status_verifikasi) VALUES('"+org_email+"','"+website+"','"+org_name+"','"+provinsi+"','"+kabupaten+"','"+kecamatan+"','"+kelurahan+"','"+kodepos+"','"+status_verifikasi+"')")


            request.session["email"] = adm_email
            request.session["name"] = adm_name
            request.session["role"] = "pengurus"
            request.session["address"] = adm_address
            request.session["logged_in"] = True
            request.session.modified = True

            messages.success(request,"Anda berhasil registrasi organisasi")
            return redirect(reverse('core:index'))
        return render(request, 'register_organisasi.html')
    messages.warning(request,"Anda sudah terdaftar dalam sistem. Silahkan logout terlebih dahulu!")
    return redirect(reverse('core:index'))

def profile_user(request):
    if 'logged_in' not in request.session or not request.session['logged_in']:
        return render(request, 'core_register.html')
    else:
        cursor = connection.cursor()
        if(request.session['role'] == 'donatur'):
            email = request.session.get('email')

            cursor.execute("SELECT P.nama FROM SION.PENGGUNA P WHERE P.email = '"+email+"'")
            select = cursor.fetchone()
            response['nama'] = select[0]

            cursor.execute("SELECT D.email FROM SION.DONATUR D WHERE D.email = '"+email+"'")
            select = cursor.fetchone()
            response['email'] = select[0]

            cursor.execute("SELECT P.alamat_lengkap FROM SION.DONATUR D, SION.PENGGUNA P WHERE P.email = D.email AND D.email = '"+email+"'")
            select = cursor.fetchone()
            response['alamat'] = select[0]

            cursor.execute("SELECT D.saldo FROM SION.DONATUR D WHERE D.email = '"+email+"'")
            select = cursor.fetchone()
            response['saldo'] = select[0]

            return render(request, 'profile_donator_sponsor.html', response)

        elif(request.session['role'] == 'sponsor'):
            email = request.session.get('email')

            cursor.execute("SELECT P.nama FROM SION.SPONSOR S, SION.PENGGUNA P WHERE S.email = P.email AND S.email = '"+email+"'")
            select = cursor.fetchone()
            response['nama'] = select[0]

            cursor.execute("SELECT S.email FROM SION.SPONSOR S WHERE S.email = '"+email+"'")
            select = cursor.fetchone()
            response['email'] = select[0]

            cursor.execute("SELECT P.alamat_lengkap FROM SION.SPONSOR S, SION.PENGGUNA P WHERE S.email = P.email AND S.email = '"+email+"'")
            select = cursor.fetchone()
            response['alamat'] = select[0]

            return render(request, 'profile_user.html', response)

        elif(request.session.role == 'relawan'):
            email = request.session.get('email')

            cursor.execute("SELECT P.nama FROM SION.RELAWAN R, SION.PENGGUNA P WHERE P.email = R.email AND R.email = '"+email+"'")
            select = cursor.fetchone()
            response['nama'] = select[0]

            cursor.execute("SELECT R.email FROM SION.RELAWAN R WHERE R.email = '"+email+"'")
            select = cursor.fetchone()
            response['email'] = select[0]

            cursor.execute("SELECT P.alamat_lengkap FROM SION.RELAWAN R, SION.PENGGUNA P WHERE R.email = P.email AND R.email = '"+email+"'")
            select = cursor.fetchone()
            response['alamat'] = select[0]

            return render(request, 'profile_user.html', response)

        elif(request.session.role == 'pengurus'):
            email = request.session.get('email')

            cursor.execute("SELECT P.nama FROM SION.PENGURUS_ORGANISASI O, SION.PENGGUNA P WHERE O.email = R.email AND O.email = '"+email+"'")
            select = cursor.fetchone()
            response['nama'] = select[0]

            cursor.execute("SELECT O.email FROM SION.PENGURUS_ORGANISASI O WHERE O.email = '"+email+"'")
            select = cursor.fetchone()
            response['email'] = select[0]

            response['alamat'] = cursor.execute("SELECT P.alamat_lengkap FROM SION.PENGURUS_ORGANISASI O, SION.PENGGUNA P WHERE O.email = P.email AND O.email = '"+email+"'")
            select = cursor.fetchone()
            response['alamat'] = select[0]
            return render(request, 'profile_user.html', response)

def profile_donator_sponsor(request):
    return render(request, 'profile_donator_sponsor.html')

def profile_search(request):
    if 'logged_in' not in request.session or not request.session['logged_in']:
        if(request.method == 'POST'):
            cursor = connection.cursor()
            cursor.execute("SELECT Nama from SION.ORGANISASI")
            selectnama = cursor.fetchone()
            response['selectnama'] = selectnama
    return render(request, 'profile_search.html')

def profile_org(request):
    if 'logged_in' not in request.session or not request.session['logged_in']:
        if(request.method == 'GET'):
            response['nama_org'] = cursor.select("SELECT nama FROM SION.ORGANISASI")
            render(request, 'profile_search.html', response)
            if(selected_org != null):

                cursor.execute("SELECT nama from SION.ORGANISASI where nama='selected_org'")
                select = cursor.fetchone()
                response['name'] = select[0]

                cursor.execute("SELECT email from SION.ORGANISASI where nama='selected_org'")
                select = cursor.fetchone()
                response['email'] = select[0]

                cursor.execute("SELECT email from SION.ORGANISASI where nama='selected_org'")
                select = cursor.fetchone()
                response['website'] = request.GET["website"]

                cursor.execute("SELECT kelurahan from SION.ORGANISASI where nama='selected_org'")
                select = cursor.fetchone()
                response['kelurahan'] = select[0]

                cursor.execute("SELECT kecamatan from SION.ORGANISASI where nama='selected_org'")
                select = cursor.fetchone()
                response['kecamatan'] = select[0]

                cursor.execute("SELECT kabupaten from SION.ORGANISASI where nama='selected_org'")
                select = cursor.fetchone()                
                response['kabupaten'] = select[0]

                cursor.execute("SELECT provinsi from SION.ORGANISASI where nama='selected_org'")
                select = cursor.fetchone()
                response['provinsi'] = select[0]

                cursor.execute("SELECT kodepos from SION.ORGANISASI where nama='selected_org'")
                select = cursor.fetchone()
                response['kodepos'] = select[0]

                cursor.execute("SELECT tujuan from SION.TUJUAN_ORGANISASI where organisasi='selected_org'")
                select = cursor.fetchone()
                response['tujuan_organisasi'] = select[0]

                cursor.execute("SELECT email from SION.PENGURUS_ORGANISASI where organisasi='selected_org'")
                select = cursor.fetchone()
                cursor.execute("SELECT nama from SION.PENGGUNA where email='select'")
                select = cursor.fetchone()
                nama2={}
                listnama = []
                for elem in select :
                    listnama.add(elem)
                nama2['nama_pengurus'] = listnama

                cursor.execute("SELECT email from SION.PENGURUS_ORGANISASI where organisasi='selected_org'")
                select = cursor.fetchone()
                listemail = []
                for elem in select :
                    listemail.add(elem)
                nama2['email_pengurus'] = select

                cursor.execute("SELECT email from SION.PENGURUS_ORGANISASI where organisasi='selected_org'")
                select = cursor.fetchone()
                cursor.execute("SELECT alamat_lengkap from SION.PENGGUNA where email='select'")
                select = cursor.fetchone()
                listalamat = []
                for elem in select :
                    listalamat.add(elem)
                nama2['alamat_pengurus'] = select

                response['penguruslist'] = nama2


    return render(request, 'profile_org.html', response)

def donasi_org(request):
    if 'logged_in' not in request.session or not request.session['logged_in']:
        if(request.method == 'POST'):
            cursor = connection.cursor()
            cursor.execute("SELECT nama from SION.ORGANISASI")
            selectnama = cursor.fetchone()
            response['selectnama'] = selectnama
            
            cursor.execute("SELECT nominal from SION.DONATUR_ORGANISASI WHERE organisasi = 'selected_name'")
            donasi = cursor.fetchone()
            donasi-=nominal
            cursor.execute("UPDATE DONATUR_ORGANISASI SET nominal = donasi WHERE organisasi = 'selected_name'")
    return render(request, 'donasi_org.html')


def get_requests(request):
    global selected_name
    global nominal
    global selected_org
    selected_name = request.POST.get('selected_name')
    nominal = request.POST.get('nominal')
    selected_org = request.POST.get('selected_org')