# SION

Kelompok 4 Tugas Kelompok

Basis Data FASILKOM UI, Semester Genap 2017/2018

Repositori _git_ ini berisi kode sumber untuk aplikasi _SION_,
yang dibuat oleh kelompok 4 yang beranggotakan:

- 4A
  - Azhar Kurnia (1606918471) dengan fitur nomor 1 dan 2
  - Andrew Savero (1606883101) dengan fitur nomor 3 dan 4
  - Ario Seto (1606917670) dengan fitur nomor 5 dan 6
  
  
## Status

Berikut adalah informasi detil tentang _branch_ utama setiap kelompok:

- SION
  - Situs: -
  - [![pipeline status](https://gitlab.com/azharkurnia/basdat-tk-4/badges/master/pipeline.svg)](https://gitlab.com/azharkurnia/basdat-tk-4/commits/master)
  - [![coverage report](https://gitlab.com/azharkurnia/basdat-tk-4/badges/master/coverage.svg)](https://gitlab.com/azharkurnia/basdat-tk-4/commits/master)
  
## Catatan Untuk Pengembang (Anggota Kelompok)

### Memulai untuk pertama kalinya

**Cobalah untuk me-_refresh_ pengetahuan Anda tentang _Django_ dari repositori praktikum PPW Anda.**
Versi dari _Django_ yang digunakan sama persis dengan versi yang digunakan pada praktikum (1.11.4). Disediakan juga
_Selenium_ bagi yang ingin melakukan _functional test_.

Anda bisa mulai pekerjaan Anda dengan mengubah isi dari _static, views, urls, templates_ yang ada pada aplikasi
``sion_core``. Isi awal dari aplikasi tersebut merupakan contoh untuk memahami struktur proyek yang digunakan.

### Konvensi dalam proyek ini

Konvensi yang sudah jelas seperti penamaan variabel harus rapih dan dapat dimengerti orang lain tidak akan ditulis disini.
Harap kerjasamanya :)

- Seharusnya, Anda tidak perlu membuat aplikasi baru lagi (cukup ``sion_core`` saja). Namun apabila terdapat kondisi
yang mengharuskan Anda untuk membuat aplikasi baru (_readability_, _maintainability_, _requirement_, dll) Anda dapat melakukan
diskusi dengan kelompok masing-masing terlebih dahulu.

  - Format nama untuk aplikasi adalah ``sion_namaaplikasi`` dengan ``namaaplikasi`` sebagai nama aplikasi yang ingin Anda buat.
  **Penamaan ini sangat penting dilakukan**, karena aplikasi _coverage_ hanya akan menganalisa baris kode yang ada didalam aplikasi berawalan ``sion_``.
  
- **Jangan langsung push ke _branch_ utama maupun _develop_ apalagi _master_!**
  
  - Buatlah _merge request_ ke _branch_ tersebut dan tunggu teman Anda me-_review_ pekerjaan Anda. Lakukan seperti
  Anda mengerjakan praktikum Pemrograman Lanjut (_Advanced Programming_).
  
- Format penamaan _branch_ fitur adalah ``namabranch`` dengan ``namabranch`` sebagai nama _branch_ yang ingin Anda buat.
  Apabila Anda tidak mengerti caranya, buatlah _branch_ dengan perintah berikut:
  
   `git checkout -b namabranch master`
  
   Contoh, Azhar kelompok kecil A ingin membuat _branch_ fitur register, maka Fulan akan memanggil perintah berikut:
  
   `git checkout -b register master`
  
   **Salah membuat cabang dari _branch_ dapat menyebabkan _error_.**

- _CI Pipeline_ terdiri dari 2 fase: **_test_** dan **_deployment_**. Fase _test_ akan dijalankan di setiap _branch_, sedangkan _deployment_
hanya akan berjalan di 2 _branch_ utama dan _develop_.

- **Konvensi di atas dibuat untuk mempermudah dalam menyelesaikan tahap 1 dari proyek ini.**
Masih banyak hal lain yang harus diperhatikan, namun belum sempat ditulis disini.
Konvensi-konvensi tambahan akan ditulis di dalam grup LINE.

### Saran

- Untuk mempermudah integrasi, sebisa mungkin gunakan format penamaan ``namafitur_namaobjek`` untuk nama _template_,
_method view_, _static file_, dan lain-lain. 

- Bagi yang ingin membuat struktur kode yang rapih bisa gunakan IDE seperti _PyCharm_. IDE dapat menotifikasi _coding style_ yang salah
(tidak memenuhi _PEP 8_ : https://pep8.org/).


### Informasi lebih lanjut

Apabila ada permasalahan dalam _pipeline_ (_error_ dalam _deployment_, bukan _unit test_ yang gagal karena kesalahan pembuat kode),
Anda dapat menghubungi Azhar.


_Good luck and have fun!_
-azharkurnia